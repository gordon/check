from django.views.generic import ListView, DetailView
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404

from .models import Session, Answer, Category, CheckList, ANSWER_VALUES
from .decorators import json_response


class HomeView(ListView):
    model = Session

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(user=self.request.user)
        if not self.request.GET.get('all', '0'):
            qs = qs.filter(active=True)
        return qs

    @classmethod
    def as_view(cls, *args, **kwargs):
        return login_required(super().as_view(*args, **kwargs))


class SessionView(DetailView):
    model = Session

    def get_queryset(self):
        qs = super().get_queryset()
        qs = qs.filter(user=self.request.user)
        return qs

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        # storing contextual data relative to the session in the objects
        context['categories'] = []
        for obj in context['object'].categories_list():
            obj.checked_boxes, obj.total_boxes = (
                context['object'].checked_boxes_for_category(obj),
                context['object'].total_boxes_for_category(obj)
            )
            obj.checklists = []
            for cl in obj.checklist_list():
                cl.checked_boxes, cl.total_boxes = (
                    context['object'].checked_boxes_for_checklist(cl),
                    context['object'].total_boxes_for_checklist(cl)
                )
                obj.checklists.append(cl)
            context['categories'].append(obj)
        return context

    @classmethod
    def as_view(cls, *args, **kwargs):
        return login_required(super().as_view(*args, **kwargs))


class SessionCategoryView(DetailView):
    model = Category

    def get_queryset(self):
        session = Session.objects.get(
            slug=self.kwargs['session'],
            user=self.request.user
        )
        self.session = session
        qs = session.enabled_categories.all()
        return qs

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['session'] = self.session
        # storing contextual data relative to the session in the objects
        context['checklists'] = []
        for obj in context['object'].checklist_list():
            obj.checked_boxes, obj.total_boxes = (
                self.session.checked_boxes_for_checklist(obj),
                self.session.total_boxes_for_checklist(obj)
            )
            context['checklists'].append(obj)
        return context

    @classmethod
    def as_view(cls, *args, **kwargs):
        return login_required(super().as_view(*args, **kwargs))


class SessionChecklistView(DetailView):
    model = CheckList

    def get_queryset(self):
        session = Session.objects.get(
            slug=self.kwargs['session'],
            user=self.request.user
        )
        self.session = session
        qs = super().get_queryset()
        qs = qs.filter(category__in=session.enabled_categories.all())
        return qs

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['session'] = self.session
        context['checked_boxes'] = \
            self.session.checked_boxes_for_checklist(context['object'])
        context['total_boxes'] = \
            self.session.total_boxes_for_checklist(context['object'])

        context['checkboxes'] = []
        answers = \
            self.session.answer_set.filter(checkbox__checklist=self.object)
        answer_dict = {a.checkbox.slug: a for a in answers}
        for checkbox in context['object'].checkbox_set.all():
            checkbox.answer = answer_dict.get(checkbox.slug)
            context['checkboxes'].append(checkbox)
        return context

    @classmethod
    def as_view(cls, *args, **kwargs):
        return login_required(super().as_view(*args, **kwargs))


@json_response
def session_checkbox_save(request, session, checkbox):
    session = get_object_or_404(Session, slug=session)
    checkbox = get_object_or_404(session.get_checkboxes(), slug=checkbox)
    value = request.POST.get('value')
    allowed_values = ANSWER_VALUES.keys()
    if value not in allowed_values:
        return {'success': 0, 'error': 'Forbidden value'}
    session.save_answer(checkbox, value)
