from django.urls import path
from django.contrib.auth.views import login, logout

from . import views

urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    path('session/<slug:slug>', views.SessionView.as_view(), name='session'),
    path('session/<slug:session>/<slug:slug>',
         views.SessionCategoryView.as_view(), name='session_category'),
    path('session/<slug:session>/checklist/<slug:slug>',
         views.SessionChecklistView.as_view(), name='session_checklist'),
    path('login', login, name='login'),
    path('logout', logout, name='logout'),
]
